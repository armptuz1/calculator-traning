package com.plusitsolution.training.basicws.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatorController {

private List<String> history = new ArrayList<String>();
	
	@RequestMapping(path = "/plus", method = RequestMethod.POST)
	public float Plus(@RequestParam("Number 1 : ") float number1,@RequestParam("Number 2 : ")float number2) {
		float result = number1 + number2;
		history.add(number1 + " + " + number2 + " = " + result);
		return result;
	}

	@RequestMapping(path = "/minus", method = RequestMethod.POST)
	public float Minus(@RequestParam("Number 1 : ") float number1,@RequestParam("Number 2 : ")float number2) {
		float result = number1 - number2;
		history.add(number1 + " - " + number2 + " = " + result);
		return result;
	}
	
	@RequestMapping(path = "/multiple", method = RequestMethod.POST)
	public float Multiple(@RequestParam("Number 1 : ") float number1,@RequestParam("Number 2 : ")float number2) {
		float result =  number1 * number2;
		history.add(number1 + " * " + number2 + " = " + result);
		return result;
	}
	
	@RequestMapping(path = "/divide", method = RequestMethod.POST)
	public float Divide(@RequestParam("Number 1 :")float number1, @RequestParam("Number 2 :")float number2) {
		float result =  number1 / number2;
		history.add(number1 + " / " + number2 + " = " + result);
		return result;
	}
	
	
	@RequestMapping(path = "/history", method = RequestMethod.GET)
	public List<String> History() {
		return history;
	}

}

